import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChatRoomComponent } from './pages';

const routes: Routes = [
    {
        path: '',
        component: ChatRoomComponent
    },
    {
        path: ':id',
        component: ChatRoomComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
