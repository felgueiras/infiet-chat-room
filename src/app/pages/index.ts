import { ChatRoomComponent } from './chat-room/chat-room.component';

export * from './chat-room/chat-room.component';

export const PAGES = [
    ChatRoomComponent
];