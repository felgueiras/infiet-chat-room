import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { ChatRoomMessage } from 'src/app/models';
import { ChatRoomService, SessionUserService } from 'src/app/services';

@Component({
    templateUrl: './chat-room.component.html',
    styleUrls: ['./chat-room.component.scss']
})
export class ChatRoomComponent implements OnInit {
    private chatEntriesSubscription: Subscription | null = null;
    
    sessionUserId: string | undefined;
    chatRoomId: string | undefined;
    chatRoomMessages$: Subject<ChatRoomMessage[]> = new Subject<ChatRoomMessage[]>();

    constructor(private route: ActivatedRoute, private chatService: ChatRoomService, private sessionUserService: SessionUserService, private cd: ChangeDetectorRef) { 
        this.sessionUserId = sessionUserService.sessionUser()?.id;
    }

    ngOnInit(): void {
        this.route.params.subscribe(p => this.updateChatRoom(p['id']));
    }

    private updateChatRoom(id: string | undefined): void {
        this.chatRoomId = id;
        if (!id) {
            return;
        }

        if (this.chatEntriesSubscription) {
            this.chatEntriesSubscription.unsubscribe();
        }

        // subscribe entries for the provided chat room
        this.chatEntriesSubscription = this.chatService.chatRoomMessage(id).subscribe(res => {
            this.chatRoomMessages$.next(res);
            this.cd.detectChanges();
        });
    }

    async onSendMessage(message: string): Promise<void> {

        if (!this.chatRoomId || !message || message.trim() === '') {
            return;
        }

        await this.chatService.saveMessage(this.chatRoomId, message);
    }
}
