export interface ChatRoomMessage {
    id: string;
    username: string;
    userId: string;
    message: string;
    timestamp: Date;
}