
export interface SessionUser {
    id: string;
    username: string;
}