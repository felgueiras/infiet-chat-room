import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ChatRoom } from 'src/app/models';

@Component({
    selector: 'app-chat-rooms-list',
    templateUrl: './chat-rooms-list.component.html',
    styleUrls: ['./chat-rooms-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatRoomsListComponent{
    @Input() chatRooms: ChatRoom[] | null = null;
}
