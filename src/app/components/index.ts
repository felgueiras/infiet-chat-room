import { ChatComponent } from './chat/chat.component';
import { ChatRoomsListComponent } from './chat-rooms-list/chat-rooms-list.component';

export * from './chat/chat.component';
export * from './chat-rooms-list/chat-rooms-list.component';

export const COMPONENTS = [
    ChatComponent,
    ChatRoomsListComponent
]