import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatRoomMessage } from 'src/app/models';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatComponent {

    messageFormControl: FormControl;

    @Input() sessionUserId: string | undefined;
    @Input() chatRoomMessage: ChatRoomMessage[] | null = [];
    @Output() sendMessage: EventEmitter<string> = new EventEmitter();
    @ViewChild('messages') messages: ElementRef<HTMLElement> | undefined;

    constructor() { 
        this.messageFormControl = new FormControl('');
    }

    sendMessageAction(): void {

        if (this.messageFormControl.invalid || !this.messageFormControl.value || this.messageFormControl.value.trim() === '') {
            return;
        }

        this.sendMessage.emit(this.messageFormControl.value.trim());

        this.messageFormControl.reset();

        if (this.messages) {
            this.messages.nativeElement.scrollTo({ behavior: 'smooth', top: this.messages.nativeElement.scrollHeight });
        }
    }
}
