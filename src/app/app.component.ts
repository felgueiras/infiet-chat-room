
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ChatRoom } from 'src/app/models';
import { ChatRoomService, SessionUserService } from 'src/app/services';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {

    chatsRooms$: Observable<ChatRoom[]>;

    constructor(private chatService: ChatRoomService, private sessionUserService: SessionUserService, private router: Router) {
        this.chatsRooms$ = chatService.chatRooms();
        this.sessionUserService.setupSessionUser();
    }

    async createNewChatRoomAction() : Promise<void> {
        const res = prompt("Chatroom name:");

        if (!res || res.trim() === '') {
            return;
        }

        const chatroomId = await this.chatService.createChatRoom(res);

        this.router.navigate([chatroomId]);
    }
}
