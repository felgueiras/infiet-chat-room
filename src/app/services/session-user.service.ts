import { Injectable } from "@angular/core";
import { SessionUser } from "../models";
import { v4 as uuid } from 'uuid';

@Injectable({
    providedIn: 'root'
})
export class SessionUserService {

    setupSessionUser(): void {
        if (this.sessionUser()) {
            return;
        }

        let username = prompt('Set your username', this.getRandomNames());

        if (!username) {
            username = this.getRandomNames();
        }

        this.saveSessionUser(username);
    }

    sessionUser(): SessionUser | undefined {
         const sessionUserJson = localStorage.getItem('session-user');

         if (!sessionUserJson) {
             return undefined;
         }

         return JSON.parse(sessionUserJson);
    }

    private saveSessionUser(username: string) {

        const sessionUser: SessionUser = {
            id: uuid(),
            username
        }

        localStorage.setItem('session-user', JSON.stringify(sessionUser));
    }

    private getRandomNames() : string {
        const names = ['Peter', 'Brian', 'Stewie', 'Glenn', 'Lois', 'Chris', 'Meg', 'Cleveland', 'Joe']
        const rand = Math.floor(Math.random() * names.length);
        return names[rand];
    }
}
