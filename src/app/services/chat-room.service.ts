import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ChatRoom, ChatRoomMessage } from '../models';
import * as firebase from 'firebase';
import { SessionUserService } from './session-user.service';

interface ChatRoomsDTO {
    name: string;
}

interface ChatRoomMessageDTO {
    username: string;
    userId: string;
    message: string;
    timestamp: firebase.default.firestore.Timestamp;
}

@Injectable({
    providedIn: 'root'
})
export class ChatRoomService {

    private _chats: Subject<ChatRoom[]> = new ReplaySubject<ChatRoom[]>(1);

    constructor(private firestore: AngularFirestore, private userService: SessionUserService) {

        this.firestore
            .collection('chatRooms')
            .snapshotChanges()
            .subscribe(res => {

                const chatsDTOs = res.filter(c => c.payload.type !== 'removed' && c.payload.doc.exists);

                const chats: ChatRoom[] = chatsDTOs.map(c => { 
                    const chatDTO = c.payload.doc.data() as ChatRoomsDTO;

                    return {
                        id: c.payload.doc.id, 
                        ...chatDTO
                    }
                });

                this._chats.next(chats);
            });
    }

    chatRooms(): Observable<ChatRoom[]> {
        return this._chats.asObservable();
    }

    chatRoomMessage(chatRoomId: string): Observable<ChatRoomMessage[]> {

        const subject = new Subject<ChatRoomMessage[]>();

        const unsubscribe = this.firestore
            .collection<ChatRoomMessageDTO>(`chatRooms/${chatRoomId}/messages`)
            .ref
            .orderBy('timestamp', 'asc')
            .onSnapshot({
                next: res => {
                    
                    const chatEntries = res.docs.filter(c => c.exists).map(e => {
                        
                        const data = e.data();

                        return {
                            id: e.id,
                            username: data.username,
                            userId: data.userId,
                            message: data.message,
                            timestamp:  data.timestamp ? data.timestamp.toDate() : new Date()
                        }
                    });

                    subject.next(chatEntries);
                }
            });

            return subject.pipe(
                finalize(() => {
                    // in case of no more subscribers, 
                    // the firestore snapshot will be killed   
                    if (subject.observers.length === 1) {
                        unsubscribe();
                    }
                })
            );
    }

    async createChatRoom(name: string): Promise<string> {
        const chatRoomDoc = this.firestore.collection(`chatRooms`).doc();

        await chatRoomDoc.set({ name });

        return chatRoomDoc.ref.id;
    }

    async saveMessage(chatroomId:string, message: string): Promise<void> {

        const sessionUser = this.userService.sessionUser();

        if (!sessionUser) {
            return;
        }

        const chatEntry = {
            message,
            username: sessionUser.username,
            userId: sessionUser.id,
            timestamp: firebase.default.firestore.FieldValue.serverTimestamp()
        }

        await this.firestore.collection(`chatRooms/${chatroomId}/messages`).add(chatEntry);
    }
}
