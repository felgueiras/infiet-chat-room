// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firestore: {
        apiKey: "AIzaSyBxwKDXDqMlngQ2fY6DYJ-NkhmIhvMrvkU",
        authDomain: "infiet-chat-room.firebaseapp.com",
        projectId: "infiet-chat-room",
        storageBucket: "infiet-chat-room.appspot.com",
        messagingSenderId: "7363948148",
        appId: "1:7363948148:web:88edf7f6a7f6f0025e2b21"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
